import React from "react";
import Datasheet from "./Datasheet/Datasheet";
import styled from "styled-components";
import initialData from "./Datasheet/initialData";
import randomColor from "randomcolor";
import RadarChart from "./Chart/RadarChart";
import { createGlobalStyle } from "styled-components";
import logo from "./ig_logo.png";
import chartImg from "./Chart.png";

const GlobalStyle = createGlobalStyle`
  body {
    margin:0;
    padding:0;
    font-size: 0.875rem;
    font-weight: 400;
    line-height: 1.43;
    letter-spacing: 0.01071em;
  }
`;

const NavBar = styled.div`
  z-index: 1100;
  display: flex;
  position: fixed;
  top: 0;
  left: auto;
  right: 0;
  width: 100%;
  height: 40px;
  padding: 8px 24px;
  vertical-align: middle;
  background: #0d222c;
  box-sizing: border-box;
  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2),
    0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);

  .logo {
    width: 24px;
  }

  .title {
    float: left;
    color: #fff;
    margin: 0 10px;
    font-size: 1.25rem;
    width: 300px;
  }
`;

const Image = styled.img`
  position: absolute;
  top: 29px;
  z-index: -1;
  width: 980px;
  margin-top: -20px;
  margin-left: -90px;
`;

const LeftContainer = styled.div`
  float: left;
  margin-top: 25px;
  width: 30%;
  height: 100%;
  padding: 8px;
  padding-top: 20px;
  border-right: 2px solid #ccc;
  background: #fff;
  overflow: auto;
`;

const RightContainer = styled.div`
  float: right;
  margin-top: 73px;
  padding: 10px;
  overflow: auto;
  width: 65%;
  height: 100%;
`;

const DatasheetWrapper = styled.div`
  position: relative;
  width: 95%;
  height: 60vh;
  border: 1px solid #eee;
  overflow: auto;
  display: block;
  margin: 20px auto;
`;

const ListWrapper = styled.div`
  position: relative;
  overflow: auto;
  width: 95%;
  height: 70px;
  display: block;
  margin: 10px auto;
`;

const ChartWrapper = styled.div`
  width: 800px;
  margin: 0 auto;
`;

const Button = styled.button`
  border: 1px solid #ddd;
  border-radius: 0;
  font-size: 18px;
  &:active {
    font-weight: 800;
  }
  &:focus {
    outline: none;
  }
`;

const LeftSideButton = styled.button`
  margin: 0 5px;
  border: 1px solid #ddd;
  border-radius: 0;
  &:active {
    font-weight: 800;
  }
  &:focus {
    outline: none;
  }
`;

const RightSideButton = styled.button`
  float: right;
  margin: 0 5px;
  border: 1px solid #ddd;
  border-radius: 0;
  &:active {
    font-weight: 800;
  }
  &:focus {
    outline: none;
  }
`;

const Popup = styled.div`
  position: absolute;
  transform: translate(-50%, -110%);
  padding: 5px 10px;
  border-radius: 2px;
  background-color: rgba(100, 100, 100, 0.8);
  font-size: 24px;
  color: white;
`;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      group: [],
      team: [],
      shapeData: "",
      dotData: "",
      values: initialData,
    };
    this.mousePos = [0, 0];
    this.handleMouseMove = this.handleMouseMove.bind(this);
  }

  componentDidMount() {
    document.addEventListener("mousemove", this.handleMouseMove);
  }

  componentWillUpdate() {
    const { data, values } = this.state;
    data.forEach(() => data.pop());
    data.shift();
    data.forEach(() => data.pop());
    const grid = Object.assign([], values);
    grid.shift();
    grid.map(
      (value) =>
        value[0].view &&
        data.push({
          data: {
            1: parseInt(value[12].value) / 10,
            2: parseInt(value[4].value) / 10,
            3: parseInt(value[5].value) / 10,
            4: parseInt(value[6].value) / 10,
            5: parseInt(value[7].value) / 10,
            6: parseInt(value[8].value) / 10,
            7: parseInt(value[9].value) / 10,
            8: parseInt(value[10].value) / 10,
            9: parseInt(value[11].value) / 10,
          },
          meta: {
            color: value[0].color,
            group: value[1].value,
            team: value[2].value,
            name: value[3].value,
          },
        })
    );
  }

  handleMouseMove(e) {
    this.mousePos = [e.pageX, e.pageY];
  }

  render() {
    const { values, data, shapeData, dotData } = this.state;
    const initialize = () => {
      const { values } = this.state;
      var index = values.length;
      while (values.length < 51) {
        var color = randomColor();
        while (parseInt(color.split("#")[1], 16) > 0xbbbbbb) {
          color = randomColor();
        }
        values.push([
          { readOnly: true, value: index, view: false, color },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
          { value: "" },
        ]);
        index++;
      }
    };

    const onCellsChanged = (changes) => {
      const grid = this.state.values.map((row) => [...row]);
      changes.forEach(({ cell, row, col, value }) => {
        grid[row][col] = { ...grid[row][col], value };
      });
      this.setState({ values: grid });
    };

    const createListUser = () => {
      const { team } = this.state;
      const grid = Object.assign([], this.state.values);
      grid.shift();
      return grid.map(
        (value, index) =>
          team.includes(value[2].value) && (
            <Button
              key={index}
              onClick={() => handleClickUser(value, index)}
              style={{
                background: value[0].view ? value[0].color : "#fff",
                color: value[0].view ? "#fff" : value[0].color,
              }}
            >
              {value[3].value}
            </Button>
          )
      );
    };

    const handleClickUser = (value, index) => {
      const { values } = this.state;
      if (value[0].view) {
        value[0].view = false;
        this.setState({
          ...this.state,
          values: [
            ...values.slice(0, index + 1),
            value,
            ...values.slice(index + 2, values.length),
          ],
        });
      } else {
        value[0].view = true;
        this.setState({
          ...this.state,
          values: [
            ...values.slice(0, index + 1),
            value,
            ...values.slice(index + 2, values.length),
          ],
        });
      }
    };

    const createListTeam = () => {
      const { group, team, values } = this.state;
      const grid = Object.assign([], values);
      grid.shift();
      const teams = Array.from(
        new Set(
          grid.map((cell) => {
            if (group.includes(cell[1].value)) return cell[2].value;
          })
        )
      );
      return teams.map(
        (value, index) =>
          value && (
            <Button
              key={index}
              style={{
                textDecoration: team.includes(value) ? "underline" : "none",
              }}
              onClick={() => handleClickTeam(value)}
            >
              {value}
            </Button>
          )
      );
    };

    const handleClickTeam = (value) => {
      const { team } = this.state;
      if (team.includes(value)) {
        team.splice(team.indexOf(value), 1);
      } else {
        team.push(value);
      }
      this.setState({ team });
    };

    const createListGroup = () => {
      const { group, values } = this.state;
      const grid = Object.assign([], values);
      grid.shift();
      const groups = Array.from(new Set(grid.map((cell) => cell[1].value)));
      groups.pop();
      return groups.map((value, index) => (
        <Button
          key={index}
          style={{
            textDecoration: group.includes(value) ? "underline" : "none",
          }}
          onClick={() => handleClickGroup(value)}
        >
          {value}
        </Button>
      ));
    };

    const handleClickGroup = (value) => {
      const { group } = this.state;
      if (group.includes(value)) {
        group.splice(group.indexOf(value), 1);
      } else {
        group.push(value);
      }
      this.setState({ group });
    };

    const handleClearAll = () => {
      this.setState({
        data: [],
        group: [],
        team: [],
        values: initialData,
      });
    };

    const handleClearSelect = () => {
      const { values } = this.state;
      const grid = Object.assign([], values);
      grid.shift();
      grid.map((value, index) => {
        value[0].view = false;
        this.setState({
          ...this.state,
          values: [
            ...values.slice(0, index + 1),
            value,
            ...values.slice(index + 2, values.length),
          ],
        });
      });
    };

    const addRow = () => {
      const { values } = this.state;
      var index = values.length;
      var color = randomColor();
      while (parseInt(color.split("#")[1], 16) > 0xbbbbbb) {
        color = randomColor();
      }
      values.push([
        { readOnly: true, value: index, view: false, color },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
        { value: "" },
      ]);
    };

    const captions = {
      // columns
      1: "후원",
      2: "신뢰",
      3: "의사결정",
      4: "인도",
      5: "중요성",
      6: "변경",
      7: "팀규모",
      8: "경험",
      9: "접근",
    };

    return (
      <React.Fragment>
        <GlobalStyle />
        <NavBar>
          <img className="logo" src={logo} />{" "}
          <span className="title">Radar</span>
        </NavBar>
        {initialize()}
        <LeftContainer>
          <ListWrapper>그룹: {createListGroup()}</ListWrapper>
          <ListWrapper>팀: {createListTeam()}</ListWrapper>
          <ListWrapper>이름: {createListUser()}</ListWrapper>
          <LeftSideButton onClick={addRow}>행추가</LeftSideButton>
          <RightSideButton onClick={handleClearSelect}>
            선택 초기화
          </RightSideButton>
          <RightSideButton onClick={handleClearAll}>
            전체 초기화
          </RightSideButton>
          <DatasheetWrapper>
            <Datasheet grid={values} onCellsChanged={onCellsChanged} />
          </DatasheetWrapper>
        </LeftContainer>
        <RightContainer>
          <ChartWrapper>
            <Image src={chartImg} />
            <RadarChart
              captions={captions}
              data={data}
              size={800}
              options={{
                dots: true,
                captions: false,
                shapeProps: () => ({
                  className: "shape",
                  mouseEnter: (shape) => {
                    this.setState({ shapeData: shape });
                  },
                  mouseLeave: (shape) => {
                    this.setState({ shapeData: "" });
                  },
                }),
                dotProps: () => ({
                  className: "dot",
                  mouseEnter: (dot) => {
                    console.log(dot);
                    this.setState({ dotData: dot });
                  },
                  mouseLeave: (shape) => {
                    this.setState({ dotData: "" });
                  },
                }),
              }}
            />
            {shapeData && (
              <Popup style={{ left: this.mousePos[0], top: this.mousePos[1] }}>
                그룹: {data[shapeData.idx] && data[shapeData.idx].meta.group}
                <br />
                &nbsp;&nbsp;&nbsp;팀:{" "}
                {data[shapeData.idx] && data[shapeData.idx].meta.team}
                <br />
                이름: {data[shapeData.idx] && data[shapeData.idx].meta.name}
              </Popup>
            )}
            {dotData && (
              <Popup style={{ left: this.mousePos[0], top: this.mousePos[1] }}>
                {dotData.value * 10} 점
              </Popup>
            )}
          </ChartWrapper>
        </RightContainer>
      </React.Fragment>
    );
  }
}

export default App;
