import ReactDataSheet from "react-datasheet";
import React from "react";
import "./react-datasheet.css";

const Datasheet = ({ grid, onCellsChanged }) => {
  return (
    <ReactDataSheet
      data={grid}
      valueRenderer={cell => cell.value}
      onContextMenu={(e, cell, i, j) =>
        cell.readOnly ? e.preventDefault() : null
      }
      onCellsChanged={changes => {
        onCellsChanged(changes);
      }}
    />
  );
};

export default Datasheet;
