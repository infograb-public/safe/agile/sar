const grid = [
  [
    { readOnly: true, value: "" },
    { value: "그룹", readOnly: true },
    { value: "팀", readOnly: true },
    { value: "이름", readOnly: true },
    { value: "후원", readOnly: true },
    { value: "신뢰", readOnly: true },
    { value: "의사결정", readOnly: true },
    { value: "인도", readOnly: true },
    { value: "중요성", readOnly: true },
    { value: "변경", readOnly: true },
    { value: "팀 규모", readOnly: true },
    { value: "경험", readOnly: true },
    { value: "접근", readOnly: true }
  ]
];

export default grid;
